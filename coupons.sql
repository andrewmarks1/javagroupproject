DROP TABLE coupons;
DROP SEQUENCE coupon_id_seq;


CREATE OR REPLACE TYPE COUPONS_TYPE FORCE AS OBJECT (
    Name            VARCHAR2(20),
    Percentage      NUMBER(10, 2)
);
/
CREATE TABLE coupons (
    couponId CHAR(4) PRIMARY KEY,
    name  VARCHAR2(20),
    percentage NUMBER(10, 2)
);
/
CREATE SEQUENCE coupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NOCACHE
    NOCYCLE;
/    
CREATE OR REPLACE TRIGGER coupon_id_trigger
BEFORE INSERT ON coupons
FOR EACH ROW
BEGIN
    SELECT 'C' || TO_CHAR(coupon_id_seq.NEXTVAL, 'FM000')
    INTO :New.couponId
    FROM DUAL;
END;
/

--add inserts  
INSERT INTO coupons (name, percentage) VALUES ('PizzaComboDeal', 15.00);
INSERT INTO coupons (name, percentage) VALUES ('FamilyNightSpecial', 20.00);
INSERT INTO coupons (name, percentage) VALUES ('StudentDiscount', 10.00);
INSERT INTO coupons (name, percentage) VALUES ('WeekendFeast', 25.00);
INSERT INTO coupons (name, percentage) VALUES ('DoubleCheeseDelight', 18.00);
INSERT INTO coupons (name, percentage) VALUES ('LoyaltyReward', 5.00);
INSERT INTO coupons (name, percentage) VALUES ('PizzaPartyPackage', 30.00);
INSERT INTO coupons (name, percentage) VALUES ('DateNightDeal', 12.00);
/
CREATE OR REPLACE PACKAGE ViewCoupons AS
    FUNCTION getAllCoupons RETURN SYS_REFCURSOR;
    PROCEDURE add_COUPON(vcoupon IN COUPONS_TYPE);
    couponExists EXCEPTION;
END ViewCoupons;
/
CREATE OR REPLACE PACKAGE BODY ViewCoupons AS
    FUNCTION getAllCoupons RETURN SYS_REFCURSOR
    IS
        coupons_cursor SYS_REFCURSOR;
    BEGIN
        OPEN coupons_cursor FOR
            SELECT name, percentage FROM coupons;

        RETURN coupons_cursor;
    END;

    PROCEDURE add_coupon(vcoupon IN COUPONS_TYPE)
    AS
        couponDoesExist NUMBER;
    BEGIN
        SELECT COUNT(name) INTO couponDoesExist
        FROM coupons
        WHERE Name = vcoupon.Name;
        
        IF couponDoesExist > 0 THEN
            RAISE couponExists;
        ELSE
            INSERT INTO coupons( Name, Percentage)
            VALUES(vcoupon.Name, vcoupon.Percentage);
        END IF;
        
    EXCEPTION
        WHEN couponExists THEN
            DBMS_OUTPUT.PUT_LINE('Product Already Exists');
            RAISE;
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
            RAISE;
    END;
END ViewCoupons;

/

