package pizzasystem;

import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

public class FilterTest {


    /**
 * Test method for filtering pizzas by size.
 * Verifies that the Filter class correctly filters pizzas by the specified size.
 * @Author Andrew Marks
 */
    @Test
    public void testFilterBySize(){

        Filter filter = new Filter();

        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        pizzas.add(new Meat("Chicken", "L", toppings1, 12.99, false));
        pizzas.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings1, 6.99, false));

        pizzas1.add(new Meat("Chicken", "L", toppings1, 12.99, false));
        

        pizzas = filter.filterBySize(pizzas, "L");
        assertEquals(pizzas1.toString(), pizzas.toString());
    }


    /**
 * Test method for filtering pizzas by type.
 * Verifies that the Filter class correctly filters pizzas by the specified type.
 * @Author Andrew Marks
 */
    @Test
    public void testFilterByType(){
        Filter filter = new Filter();

        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        pizzas.add(new Vegetarian("Nothing", "L", null, 12.99, false));
        pizzas.add(new Vegetarian("Nothing", "M", null, 9.99, false));
        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings1, 6.99, false));

        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas1.add(new Meat("Bacon", "S", toppings1, 6.99, false));
        

        pizzas = filter.filterByType(pizzas, "Meat");
        assertEquals(pizzas1.toString(), pizzas.toString());
    }


    /**
 * Test method for filtering pizzas by name.
 * Verifies that the Filter class correctly filters pizzas by the specified name.
 * @Author Andrew Marks
 */
    @Test
    public void testFilterByName(){
        Filter filter = new Filter();

        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        pizzas.add(new Vegetarian("Nothing", "L", null, 12.99, false));
        pizzas.add(new Vegetarian("Nothing", "M", null, 9.99, false));
        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings1, 6.99, false));

        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));

        pizzas = filter.filterByName(pizzas, "Chicken");
        assertEquals(pizzas1.toString(), pizzas.toString());
    }


/**
     * Test method for filtering pizzas by topping.
     * Verifies that the Filter class correctly filters pizzas by the specific topping
     * @Author Andrew Marks
     */
    @Test
    public void testFilterByTopping(){
        Filter filter = new Filter();

        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        
        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings2, 6.99, true));

        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));

        

        pizzas = filter.filterByTopping(pizzas, "chicken");
        assertEquals(pizzas1.toString(), pizzas.toString());
    }


  /**
 * Test method for filtering pizzas by price.
 * Verifies that the Filter class correctly filters pizzas by the specified price.
 * @Author Andrew Marks
 */
    @Test
    public void testFilterByPrice(){
        Filter filter = new Filter();

        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        
        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings2, 9.99, true));

        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));

        

        pizzas = filter.filterByPrice(pizzas, 6.99);
        assertEquals(pizzas1.toString(), pizzas.toString());
    }
    
/**
 * Test method for filtering pizzas by gluten.
 * Verifies that the Filter class correctly filters pizzas based on gluten-free status.
 */
    @Test
    public void testFilterByGluten(){
        Filter filter = new Filter();

        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("mushroom");

        pizzas.add(new Vegetarian("Mushroom", "L", toppings2, 12.99, true));
        pizzas.add(new Vegetarian("Mushroom", "L", toppings2, 12.99, false));
        pizzas.add(new Vegetarian("Mushroom", "L", toppings2, 12.99, false));
        pizzas.add(new Meat("Chicken", "L", toppings1, 9.99, true));


        pizzas1.add(new Vegetarian("Mushroom", "L", toppings2, 12.99, true));
        pizzas1.add(new Meat("Chicken", "L", toppings1, 9.99, true));

        pizzas = filter.filterByGluten(pizzas);

        assertEquals(pizzas1.toString(), pizzas.toString());

    }

}
