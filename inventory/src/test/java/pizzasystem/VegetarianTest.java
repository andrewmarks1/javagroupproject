package pizzasystem;

import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

public class VegetarianTest{


    /**
 * Test method for retrieving the vegetable toppings from a Vegetarian pizza.
 * Verifies that the getVegetables method returns the correct vegetable toppings.
 * @Author Andrew Marks
 */
    @Test
    public void testGetVegetables(){
        List<String> toppings = new ArrayList<String>();
        toppings.add("pepper");
        Vegetarian vegetarian = new Vegetarian("Nothing", "L", toppings, 12.99, false);
        assertEquals("pepper", vegetarian.getVegetables());
    }


    /**
 * Test method for adding a vegetable topping to a Vegetarian pizza.
 * Verifies that the addVegetable method throws an IllegalArgumentException for invalid vegetable toppings.
 * @Author Andrew Marks
 */
    @Test(expected = IllegalArgumentException.class)
    public void testAddVegetables(){
        List<String> toppings = new ArrayList<String>();
        toppings.add("");
            Vegetarian vegetarian = new Vegetarian("Nothing", "L", toppings, 12.99, false);
        vegetarian.addVegetable("chicken");
    }


    /**
 * Test method for obtaining the string representation of a Vegetarian pizza.
 * Verifies that the toString method returns the expected string representation.
 * @Author Andrew Marks
 */
    @Test
    public void testToString(){
        List<String> toppings = new ArrayList<String>();
        toppings.add("mushroom");
        Vegetarian vegetarian = new Vegetarian("Mushroom", "L", toppings, 12.99, false);
        assertEquals("Vegetarian pizza Name:,Mushroom,L,mushroom,12.99,false", vegetarian.toString());
    }
}