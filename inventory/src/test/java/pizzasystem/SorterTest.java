package pizzasystem;

import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

public class SorterTest {


    /**
 * Test method for sorting pizzas by size in ascending order.
 * Verifies that the Sorter class correctly sorts pizzas by size in ascending order.
 * @Author Andrew Marks
 */
    @Test
    public void testSizeSorterAsc(){
        Sorter sorter = new Sorter();
        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        pizzas.add(new Meat("Chicken", "L", toppings1, 12.99, false));
        pizzas.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings1, 6.99, false));

        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas1.add(new Meat("Bacon", "S", toppings1, 6.99, false));
        pizzas1.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas1.add(new Meat("Chicken", "L", toppings1, 12.99, false));

        pizzas = sorter.sizeSorter(pizzas, "asc");
        assertEquals(pizzas1.toString(), pizzas.toString());

    }


    /**
 * Test method for sorting pizzas by size in descending order.
 * Verifies that the Sorter class correctly sorts pizzas by size in descending order.
 * @Author Andrew Marks
 */
    @Test
    public void testSizeSorterDesc(){
        Sorter sorter = new Sorter();
        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Chicken", "L", toppings1, 12.99, false));
        pizzas.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        
        

        pizzas1.add(new Meat("Chicken", "L", toppings1, 12.99, false));
        pizzas1.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas1.add(new Meat("Bacon", "S", toppings1, 6.99, false));
        

        pizzas = sorter.sizeSorter(pizzas, "desc");
        assertEquals(pizzas1.toString(), pizzas.toString());

    }


    /**
 * Test method for sorting pizzas by price in ascending order.
 * Verifies that the Sorter class correctly sorts pizzas by price in ascending order.
 * @Author Andrew Marks
 */
    @Test
    public void priceSorterAsc(){
        Sorter sorter = new Sorter();
        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        pizzas.add(new Meat("Chicken", "L", toppings1, 12.99, false));
        pizzas.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings1, 6.99, false));

        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas1.add(new Meat("Bacon", "S", toppings1, 6.99, false));
        pizzas1.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas1.add(new Meat("Chicken", "L", toppings1, 12.99, false));

        pizzas = sorter.priceSorter(pizzas, "asc");
        assertEquals(pizzas1.toString(), pizzas.toString());
    }


    /**
 * Test method for sorting pizzas by price in descending order.
 * Verifies that the Sorter class correctly sorts pizzas by price in descending order.
 * @Author Andrew Marks
 */
    @Test
    public void priceSorterDesc(){
        Sorter sorter = new Sorter();
        List<Pizza> pizzas = new ArrayList<Pizza>();
        List<Pizza> pizzas1 = new ArrayList<Pizza>();
        List<String> toppings1 = new ArrayList<String>();
        List<String> toppings2 = new ArrayList<String>();

        toppings1.add("chicken");
        toppings2.add("bacon");

        pizzas.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "S", toppings1, 6.99, false));
        pizzas.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas.add(new Meat("Chicken", "L", toppings1, 12.99, false));
       
        
        
        pizzas1.add(new Meat("Chicken", "L", toppings1, 12.99, false));
        pizzas1.add(new Meat("Bacon", "M", toppings1, 9.99, false));
        pizzas1.add(new Meat("Chicken", "S", toppings1, 6.99, false));
        pizzas1.add(new Meat("Bacon", "S", toppings1, 6.99, false));
        

        pizzas = sorter.priceSorter(pizzas, "desc");
        assertEquals(pizzas1.toString(), pizzas.toString());

    }

}
