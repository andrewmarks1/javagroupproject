package pizzasystem;

import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

public class MeatTest {

    /**
 * Test method for retrieving the meat topping from a Meat pizza.
 * Verifies that the getMeat method returns the correct meat topping.
 * @Author Andrew Marks
 */
    @Test
    public void testGetMeat(){
        List<String> toppings = new ArrayList<String>();
        toppings.add("chicken");
        Meat meat = new Meat("Chicken", "L", toppings, 12.99, false);
        assertEquals("chicken", meat.getMeat());
    }

    /**
 * Test method for adding a meat topping to a Meat pizza.
 * Verifies that the addMeat method throws an IllegalArgumentException for invalid meat toppings.
 * @Author Andrew Marks
 */
    @Test(expected = IllegalArgumentException.class)
    public void testAddMeat(){
        List<String> toppings = new ArrayList<String>();
        toppings.add("");
        Meat meat = new Meat("Chicken", "L", toppings, 12.99, false);
        meat.addMeat("pepper");
    }

    /**
 * Test method for obtaining the string representation of a Meat pizza.
 * Verifies that the toString method returns the expected string representation.
 * @Author Andrew Marks
 */
    @Test
    public void testToString(){
        List<String> toppings = new ArrayList<String>();
        toppings.add("mushroom");
        Meat meat = new Meat("Chicken", "L", toppings, 12.99, false);
        assertEquals("Meat Pizza Name:,Chicken,L,mushroom,12.99,false", meat.toString());
    }
    
}
