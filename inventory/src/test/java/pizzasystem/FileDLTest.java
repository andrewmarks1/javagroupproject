package pizzasystem;

import static org.junit.Assert.assertTrue;

import javax.imageio.IIOException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

import org.junit.Test;

public class FileDLTest{
    @Test
    /*
     * This method is to test if FileDl.addData() does what it is supposed to do
     * @Author David Hebert
     */
    public void shouldAddDataToFile() throws IIOException, IOException{

        List<String> toppings =  new ArrayList<String>();
        toppings.add("cheese");
        toppings.add("pepperoni");
        Pizza testPizza = new Meat("Pepperoni", "L", toppings, 12.99, true);
        FileDL fileDL = new FileDL();

        Path pCopy = Paths.get("../copy.txt");
        Files.createFile(pCopy);
        Path pExpected = Paths.get("../test.txt");

        try {
            fileDL.addData(testPizza, "../copy.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> expected = Files.readAllLines(pExpected, StandardCharsets.UTF_8);
        List<String> copy = Files.readAllLines(pCopy, StandardCharsets.UTF_8);

        if(copy.equals(expected)){
            Files.delete(pCopy);
            assertTrue(true);
        }
        else{
            Files.delete(pCopy);
            assertTrue(false);
        }  
    }
}