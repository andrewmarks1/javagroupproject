package pizzasystem;

import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.*;
import java.util.ArrayList.*;
import java.util.List.*;
import java.io.FileWriter;

import javax.swing.GroupLayout.SequentialGroup;

import java.sql.*;

import oracle.jdbc.OracleTypes;

/**
 * The admin password is: password
 *
 */
public class App 
{
    public static void main(String[] args) throws SQLException, ClassNotFoundException{ 
        Scanner scan = new Scanner(System.in);
        boolean perm = true;

        System.out.println("\nWelcome to the Pizza inventory System.");

        while(perm){
            try {
                System.out.println("\nIf you are an admin, please enter '1'. If you are a customer, please enter '2'.");
                int permission = scan.nextInt();
                switch(permission){
                    case 1:
                        prompt_password(scan);
                        break;
                    case 2:
                        display_customer_menu(scan);
                        break;
                    default :
                        System.out.println("Invalid Input");
                        perm = false;
                        break;
                }
            }
            catch(IllegalArgumentException | SQLException e){
                e.printStackTrace();
            }
        }
    }   


    /**
 * Prompts the user to enter a password and checks if it matches the correct password.
 *
 * @param scan A Scanner object for input.
 * @throws SQLException If a database access error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void prompt_password(Scanner scan) throws SQLException, ClassNotFoundException{
        
        String correctPassword = "password";
        String password=new String(System.console().readPassword("Password: "));
        if(password.equals(correctPassword)){
            display_admin_menu(scan);
        }
        else{
            System.out.println("Incorrect Password");
            System.exit(0);
        }
    }


    /**
 * Displays the admin menu, allowing the user to perform various admin-related tasks.
 *
 * @param scan A Scanner object for input.
 * @throws SQLException If a database access error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void display_admin_menu(Scanner scan)throws SQLException, ClassNotFoundException{
        boolean adminPerms = true;
        try{
            System.out.println("\nWelcome to the Admin Menu.");
            boolean adminMenu = true;
            while(adminMenu){
                System.out.println("\nEnter '1' to display the inventory menu");
                System.out.println("Enter '2' to search for a particular item");
                System.out.println("Enter '3' to view all coupons");
                System.out.println("Enter '4' to add to the inventory");
                System.out.println("Enter '5' to update the price of an item");
                System.out.println("Enter '6' to exit");
                int answer = scan.nextInt();


                switch (answer) {
                    case 1:
                        display_inventory(scan, adminPerms);
                        break;
                    case 2:
                        search_items(scan, adminPerms);
                        break;
                    case 3:
                        view_coupons(scan, adminPerms);
                        break;
                    case 4:
                        add_inventory(scan);
                        break;
                    case 5:
                        update_price(new FileDL(), new Filter(), scan);
                        break;
                    case 6:
                        adminMenu = false;
                        System.exit(0);
                    default :
                        System.out.println("Invalid Input");
                        break;
                }
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        catch(IOException e){
            System.out.println("Error reading all lines");
        }
    }


    /**
 * Displays the customer menu, allowing the user to perform various customer-related tasks.
 *
 * @param scan A Scanner object for input.
 * @throws SQLException If a database access error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void display_customer_menu(Scanner scan) throws SQLException, ClassNotFoundException{
        boolean adminPerms = false;
        try{
            System.out.println("\nWelcome to the customer Menu.");
            boolean adminMenu = true;
            while(adminMenu){
                System.out.println("\nEnter '1' to display the inventory menu");
                System.out.println("Enter '2' to search for a particular item");
                System.out.println("Enter '3' to view all coupons");
                System.out.println("Enter '4' to exit");
                int answer = scan.nextInt();


                switch (answer) {
                    case 1:
                        display_inventory(scan, adminPerms);
                        break;
                    case 2:
                        search_items(scan, adminPerms);
                        break;
                    case 3:
                        view_coupons(scan, adminPerms);
                        break;
                    case 4:
                        System.exit(0);
                    default :
                        System.out.println("Invalid Input");
                        break;
                }
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        catch(IOException e){
            System.out.println("Error reading all lines");
        }
    }


    /**
 * Displays the inventory menu, allowing the user to view different pizza categories.
 *
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void display_inventory(Scanner scan, boolean adminPerms) throws SQLException, ClassNotFoundException{
        try{
            System.out.println("\nWelcome to the inventory menu");
            boolean displayMenu = true;
            while(displayMenu){
                System.out.println("\nEnter '1' to display all pizzas");
                System.out.println("Enter '2' to display all Vegetarian Pizzas");
                System.out.println("Enter '3' to display all Meat Pizzas");
                System.out.println("If you are an Admin, enter '4' to go back");
                System.out.println("If you are a Customer, enter '5' to go back");
                int answer = scan.nextInt();

                switch(answer){
                    case 1:
                        display_all_pizzas(new FileDL(), new Sorter(), scan, adminPerms);
                        break;
                    case 2:
                        display_all_veg_pizzas(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                        break;
                    case 3:
                        display_all_meat_pizzas(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                        break;
                    case 4:
                        if(adminPerms){
                            display_admin_menu(scan);
                            break;
                        }
                        else{
                            System.out.println("You do not have permission.");
                        }
                    case 5:
                        display_customer_menu(scan);
                        break;
                    default :
                        System.out.println("Invalid Input");
                        break;
                }
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        catch(IOException e){
            System.out.println("Error reading all lines");
        }
    }


    /**
 * Displays all pizzas, optionally allowing sorting based on user preferences.
 *
 * @param fileDL A FileDL object for file operations.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void display_all_pizzas(FileDL fileDL, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            for(Pizza pizza : pizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by size ascending");
            System.out.println("Enter '2' to sort by size descending");
            System.out.println("Enter '3' to sort by price descending");
            System.out.println("Enter '4' to sort by price ascending");
            System.out.println("Enter '5' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    pizzas = sorter.sizeSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    pizzas = sorter.sizeSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    pizzas = sorter.priceSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 4:
                    pizzas = sorter.priceSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 5:
                    display_inventory(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


    /**
 * Displays all vegetarian pizzas, optionally allowing sorting based on user preferences.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by type.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void display_all_veg_pizzas(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            List<Pizza> vegPizzas = filter.filterByType(pizzas, "Veg");
            for(Pizza pizza : vegPizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by size ascending");
            System.out.println("Enter '2' to sort by size descending");
            System.out.println("Enter '3' to sort by price descending");
            System.out.println("Enter '4' to sort by price ascending");
            System.out.println("Enter '5' to go back");
            int answer = scan.nextInt();
            

            switch(answer){
                case 1:
                    vegPizzas = sorter.sizeSorter(vegPizzas, "asc");
                    for(Pizza p : vegPizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    vegPizzas = sorter.sizeSorter(vegPizzas, "desc");
                    for(Pizza p : vegPizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    vegPizzas = sorter.priceSorter(vegPizzas, "desc");
                    for(Pizza p : vegPizzas){
                        System.out.println(p);
                    }
                    break;
                case 4:
                    vegPizzas = sorter.priceSorter(vegPizzas, "asc");
                    for(Pizza p : vegPizzas){
                        System.out.println(p);
                    }
                    break;
                case 5:
                    display_inventory(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


    /**
 * Displays all meat pizzas, optionally allowing sorting based on user preferences.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by type.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void display_all_meat_pizzas(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            List<Pizza> meatPizzas = filter.filterByType(pizzas, "Meat");
            for(Pizza pizza : meatPizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by size ascending");
            System.out.println("Enter '2' to sort by size descending");
            System.out.println("Enter '3' to sort by price descending");
            System.out.println("Enter '4' to sort by price ascending");
            System.out.println("Enter '5' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    meatPizzas = sorter.sizeSorter(meatPizzas, "asc");
                    for(Pizza p : meatPizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    meatPizzas = sorter.sizeSorter(meatPizzas, "desc");
                    for(Pizza p : meatPizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    meatPizzas = sorter.priceSorter(meatPizzas, "desc");
                    for(Pizza p : meatPizzas){
                        System.out.println(p);
                    }
                    break;
                case 4:
                    meatPizzas = sorter.priceSorter(meatPizzas, "asc");
                    for(Pizza p : meatPizzas){
                        System.out.println(p);
                    }
                    break;
                case 5:
                    display_inventory(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


    /**
 * Displays the search menu, allowing the user to search for pizzas based on different criteria.
 *
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws IOException If an I/O error occurs.
 * @throws SQLException If a database access error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void search_items(Scanner scan, boolean adminPerms) throws IOException, SQLException, ClassNotFoundException{
        System.out.println("\nWelcome to the Search Menu.");
        boolean searchMenu = true;
        while(searchMenu){
            System.out.println("\nEnter '1' to search by name");
            System.out.println("Enter '2' to search by size");
            System.out.println("Enter '3' to search by a specific topping");
            System.out.println("Enter '4' to search by price");
            System.out.println("Enter '5' to search for gluten pizzas");
            System.out.println("If you are an Admin, enter '6' to go back");
            System.out.println("If you are a Customer, enter '7' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    search_by_name(new FileDL(), new Filter(), scan, adminPerms);
                    break;
                case 2:
                    search_by_size(scan, adminPerms);
                    break;
                case 3:
                    search_by_topping(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                    break;
                case 4:
                    search_by_price(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                    break;
                case 5:
                    search_by_gluten(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                    break;
                case 6:
                    if(adminPerms){
                        display_admin_menu(scan);
                        break;
                    }
                    else{
                        System.out.println("You do not have permission.");
                    }
                case 7:
                    display_customer_menu(scan);
                default :
                    System.out.println("Invalid Input");
                    break;
            }
        }
    }
    

    /**
 * Searches for pizzas by name and displays the matching results.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by name.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws IOException If an I/O error occurs.
 * @Author Andrew Marks
 */
    public static void search_by_name(FileDL fileDL, Filter filter, Scanner scan, boolean adminPerms) throws IOException{
        System.out.println("\nName Searcher");
        System.out.println("\nEnter the name you would like to search. \nTo go back, type 'back'");
        String name = scan.nextLine();
        name = scan.nextLine();
        try{
            if(name.equals("back")){
                search_items(scan, adminPerms);
            }
            else{
                List<Pizza> pizzas = new ArrayList<Pizza>();
                pizzas = fileDL.dataLoader();
                pizzas = filter.filterByName(pizzas, name);
                for(Pizza pizza : pizzas){
                    System.out.println(pizza);
                }
            }
        }
        catch(Exception e){
            System.out.println("Name not found");
            search_by_name(fileDL, filter, scan, adminPerms);
        } 
    }


    /**
 * Allows the user to search for pizzas based on size and optionally sort the results.
 *
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void search_by_size(Scanner scan, boolean adminPerms) throws SQLException, ClassNotFoundException{
        try{
            System.out.println("\nSize Searcher");
            System.out.println("\nEnter '1' to search for Large pizzas");
            System.out.println("Enter '2' to search for Medium pizzas");
            System.out.println("Enter '3' to search for Small pizzas");
            System.out.println("Enter '4' to go back");
            int size = scan.nextInt();

                switch(size){
                    case 1:
                        search_large(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                        break;
                    case 2:
                        search_medium(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                        break;
                    case 3:
                        search_small(new FileDL(), new Filter(), new Sorter(), scan, adminPerms);
                        break;
                    case 4:
                        search_items(scan, adminPerms);
                        break;
                    default :
                        System.out.println("Invalid Input");
                        break;
                }
            }
        catch(IOException e){
            System.out.println("Error reading all lines");
        }
    }


 /**
 * Displays all large pizzas, optionally allowing sorting based on user preferences.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by size.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */  
    public static void search_large(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        System.out.println("\nAll Large Pizzas");
        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            pizzas = filter.filterBySize(pizzas, "L");
            for(Pizza pizza : pizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by price descending");
            System.out.println("Enter '2' to sort by price ascending");
            System.out.println("Enter '3' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    pizzas = sorter.priceSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    pizzas = sorter.priceSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    search_items(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
            
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


   /**
 * Displays all medium pizzas, optionally allowing sorting based on user preferences.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by size.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
 
    public static void search_medium(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        System.out.println("\nAll Medium Pizzas");
        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            pizzas = filter.filterBySize(pizzas, "M");
            for(Pizza pizza : pizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by price descending");
            System.out.println("Enter '2' to sort by price ascending");
            System.out.println("Enter '3' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    pizzas = sorter.priceSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    pizzas = sorter.priceSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    search_items(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


    /**
 * Displays all small pizzas, optionally allowing sorting based on user preferences.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by size.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void search_small(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        System.out.println("\nAll Small Pizzas");
        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            pizzas = filter.filterBySize(pizzas, "S");
            for(Pizza pizza : pizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by price descending");
            System.out.println("Enter '2' to sort by price ascending");
            System.out.println("Enter '3' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    pizzas = sorter.priceSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    pizzas = sorter.priceSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    search_items(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


   /**
 * Allows the user to search for pizzas based on topping and optionally sort the results.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by topping.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */ 
    public static void search_by_topping(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        System.out.println("\nSearch by Topping");

        System.out.println("\nWhat topping would you like to search for?");
        String topping = scan.nextLine();
        topping = scan.nextLine();

        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            pizzas = filter.filterByTopping(pizzas, topping);
            for(Pizza pizza : pizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by size ascending");
            System.out.println("Enter '2' to sort by size descending");
            System.out.println("Enter '3' to sort by price descending");
            System.out.println("Enter '4' to sort by price ascending");
            System.out.println("Enter '5' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    pizzas = sorter.sizeSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    pizzas = sorter.sizeSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    pizzas = sorter.priceSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 4:
                    pizzas = sorter.priceSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 5:
                    search_items(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
            
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


    /**
 * Allows the user to search for pizzas based on price and optionally sort the results.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by price.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void search_by_price(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws IOException, SQLException, ClassNotFoundException{
        System.out.println("\nSearch By Price");
        List<Pizza> pizzas = new ArrayList<Pizza>();

        System.out.println("\nWhat price would you like to search for? Our prices range from 4.99-16.99");
        double price = scan.nextDouble();
        try{
            pizzas = fileDL.dataLoader();
            pizzas = filter.filterByPrice(pizzas, price);
            for(Pizza pizza : pizzas){
                System.out.println(pizza);
            }
            System.out.println("\nEnter '1' to sort by size ascending");
            System.out.println("Enter '2' to sort by size descending");
            System.out.println("Enter '3' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    pizzas = sorter.sizeSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    pizzas = sorter.sizeSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    search_items(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }


    /**
 * Allows the user to search for gluten-free pizzas and optionally sort the results.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas by gluten-free status.
 * @param sorter A Sorter object for sorting pizzas.
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws IOException If an I/O error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author Andrew Marks
 */
    public static void search_by_gluten(FileDL fileDL, Filter filter, Sorter sorter, Scanner scan, boolean adminPerms) throws SQLException, IOException, ClassNotFoundException{
        System.out.println("\nSearch by Gluten");

        try{
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            pizzas = filter.filterByGluten(pizzas);
            for(Pizza pizza : pizzas){
                System.out.println(pizza);
            }

            System.out.println("\nEnter '1' to sort by size ascending");
            System.out.println("Enter '2' to sort by size descending");
            System.out.println("Enter '3' to sort by price descending");
            System.out.println("Enter '4' to sort by price ascending");
            System.out.println("Enter '5' to go back");
            int answer = scan.nextInt();

            switch(answer){
                case 1:
                    pizzas = sorter.sizeSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 2:
                    pizzas = sorter.sizeSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 3:
                    pizzas = sorter.priceSorter(pizzas, "desc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 4:
                    pizzas = sorter.priceSorter(pizzas, "asc");
                    for(Pizza p : pizzas){
                        System.out.println(p);
                    }
                    break;
                case 5:
                    search_items(scan, adminPerms);
                    break;
                default :
                    System.out.println("Invalid input");
                    break;
            }
        }
        catch(InputMismatchException e){
            System.out.println("Invalid input");
            scan.nextLine();
        }
        finally{
            System.out.println("\nSuccessful");
        }
    }



/**
 * Displays all available coupons and allows an admin to add new coupons.
 *
 * @param scan A Scanner object for input.
 * @param adminPerms A boolean indicating whether the user has admin permissions.
 * @throws SQLException If a database access error occurs.
 * @throws ClassNotFoundException If the class is not found.
 * @Author David Hebert
 */
    public static void view_coupons(Scanner scan, Boolean adminPerms) throws SQLException, ClassNotFoundException{
        String user=System.console().readLine("UserName: ");
        String password=new String(System.console().readPassword("Password: "));
        String url ="jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";

        Connection conn = DriverManager.getConnection(url, user, password);
        System.out.println("connected to the database");
        String sql = "{ ? = call ViewCoupons.getAllCoupons() }";
        System.out.println(" Coupons");
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.registerOutParameter(1, OracleTypes.CURSOR);
            stmt.executeQuery();

            try(ResultSet rs = (ResultSet) stmt.getObject(1)){
                while (rs.next()) {
                    String name = rs.getString("name");
                    String percentage = rs.getString("percentage");

                    System.out.println("\nName: " + name + "\nPercentage: " + percentage);
                }
            }
        }
        catch (SQLException e) {
         e.printStackTrace();
        }

        Boolean answerTrue  = true;
        while(adminPerms && answerTrue){
            System.out.println("Would you like to add a coupon? y/n");
            String answer = scan.nextLine();
            answer = scan.nextLine();
            if(answer.equals("y")){
                add_coupon(scan, conn);
            }
            else if(answer.equals("n")){
                display_admin_menu(scan);
            }
        }
        
    }


    /**
 * Adds a new coupon to the database based on user input.
 *
 * @param scan A Scanner object for input.
 * @param conn A Connection object for database connection.
 * @throws ClassNotFoundException If the class is not found.
 * @throws SQLException If a database access error occurs.
 * @Author David Hebert
 */
    public static void add_coupon(Scanner scan, Connection conn) throws ClassNotFoundException, SQLException{
        try {
            if (conn != null && !conn.isClosed()) {
                System.out.println("What is the name of the coupon you would like to add?");
                String name = scan.nextLine();
                System.out.println("What discount would you like to add to the coupon? (percentages)");
                Double percentage = scan.nextDouble();

                Map map = conn.getTypeMap();
                conn.setTypeMap(map);   
                map.put("COUPONS_TYPE", Class.forName("pizzasystem.Coupon"));
                conn.setTypeMap(map);

                Coupon newCoupon = new Coupon(name, percentage);
                String sql = "{call ViewCoupons.add_coupon(?) }";
            try(CallableStatement stmt = conn.prepareCall(sql)){
                stmt.setObject(1, newCoupon);
                stmt.execute();
                System.out.println("Coupon added successfully!");
            }
            } else {
                System.out.println("Connection is closed.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
 * Adds a new pizza to the inventory based on user input.
 *
 * @param scan A Scanner object for input.
 * @throws IOException If an I/O error occurs.
 * @Author David Hebert
 */
    public static void add_inventory(Scanner scan) throws IOException {
        System.out.println("\nPlease enter the name, size, toppings, price, and is it gluten.");
        System.out.println("Name");
        String name = scan.nextLine();
        name = scan.nextLine();
        System.out.println("Size (L, M, S)");
        String size = scan.nextLine();
        System.out.println("Toppings (please add space between each topping)");
        String toppings = scan.nextLine();
        List<String> listToppings = Arrays.asList(toppings.split(" "));
        listToppings.replaceAll(String::trim);
        System.out.println("Price");
        Double price = scan.nextDouble();
        System.out.println("Gluten y/n?");
        String gluten = scan.nextLine();
        gluten = scan.nextLine();
        Boolean isGluten = true;
        if (gluten.equals("y") || gluten.equals("yes")) {
            isGluten = true;
        } else if (gluten.equals("n") || gluten.equals("no")) {
            isGluten = false;
        } else {
            System.out.println("Invalid options");
            add_inventory(scan);
        }
    
        Pizza newPizza = new Pizza(name, size, listToppings, price, isGluten);
        if (newPizza.isVegetarian()) {
            Pizza vegPizza = new Vegetarian(newPizza);
            FileDL fileDL = new FileDL();
            fileDL.addData(vegPizza, "data.txt");
        } else {
            Pizza meatPizza = new Meat(newPizza);
            FileDL fileDL = new FileDL();
            fileDL.addData(meatPizza, "data.txt");
        }
    }
    
    
/**
 * Updates the price of a pizza in the inventory based on user input.
 *
 * @param fileDL A FileDL object for file operations.
 * @param filter A Filter object for filtering pizzas.
 * @param scan A Scanner object for input.
 * @throws IOException If an I/O error occurs.
 * @Author David Hebert
 */
    public static void update_price(FileDL fileDL, Filter filter, Scanner scan) throws IOException {
        System.out.println("\nEnter the name and size of the pizza for which you want to update the price");
        System.out.println("\nName:");
        String name = scan.nextLine();
        name = scan.nextLine();
        System.out.println("\nSize (L, M, S)");
        String size = scan.nextLine();
        System.out.println("\nWhat price would you like to set");
        double price = scan.nextDouble();
        List<Pizza> newList = new ArrayList<Pizza>();

        try {
            List<Pizza> pizzas = new ArrayList<Pizza>();
            pizzas = fileDL.dataLoader();
            for (Pizza p : pizzas) {
                if (p.getName().equals(name) && p.getSize().equals(size)) {
                    p.setPrice(price);
                }
                newList.add(p);
            }
            for (Pizza p : newList) {
                System.out.println(p); 
            }
        } finally {
            System.out.println("\nSuccessful");
        }

        FileWriter fileWriter = new FileWriter("data.txt", false);
        try (PrintWriter printWriter = new PrintWriter(fileWriter)) {
            for (Pizza p : newList) {
                printWriter.println(String.format("%s pizza Name:,%s,%s,%s,%.2f,%s",
                        p.getClass().getSimpleName(),
                        p.getName(), p.getSize(), String.join(" ", p.getToppings()), p.getPrice(), p.isGlutenFree()));
            }
        }
    }
}
