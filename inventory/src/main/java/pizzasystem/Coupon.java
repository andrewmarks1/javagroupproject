package pizzasystem;
import java.sql.*;


public class Coupon implements SQLData{
    private static final String TYPENAME = "COUPONS_TYPE";

    private String name;
    private double percentage;

    /**
 * Parameterized constructor for the Coupon class.
 * Initializes a Coupon instance with the specified name and percentage values.
 *
 * @param name       The name of the coupon.
 * @param percentage The percentage discount offered by the coupon.
 * @Author David Hebert
 */
    public Coupon(String name, double percentage){
        this.name = name;
        this.percentage = percentage;
    }


    /**
 * Sets the name of the coupon.
 *
 * @param name The new name for the coupon.
 * @Author David Hebert
 */
    public void setName(String name){
        this.name = name;
    }


    /**
 * Sets the percentage discount offered by the coupon.
 *
 * @param percentage The new percentage discount.
 * @Author David Hebert
 */
    public void setPercentage(double percentage){
        this.percentage = percentage;
    }


    /**
 * Retrieves the name of the coupon.
 *
 * @return The name of the coupon.
 * @Author David Hebert
 */
    public String getName(){
        return this.name;
    }


    /**
 * Retrieves the percentage discount offered by the coupon.
 *
 * @return The percentage discount.
 * @Author David Hebert
 */
    public double getPercentage(){
        return this.percentage;
    }


    /**
 * Writes the coupon data to an SQLOutput stream.
 *
 * @param stream The SQLOutput stream to write the data to.
 * @throws SQLException If there is an error writing the data.
 * @Author David Hebert
 */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException{
        stream.writeString(getName());
        stream.writeDouble(getPercentage());
    }


    /**
 * Reads the coupon data from an SQLInput stream.
 *
 * @param stream    The SQLInput stream to read the data from.
 * @param typeName  The SQL type name of the data.
 * @throws SQLException If there is an error reading the data.
 * @Author David Hebert
 */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setName(stream.readString());
        setPercentage(stream.readDouble());
    }


    /**
 * Retrieves the SQL type name associated with the Coupon class.
 *
 * @return The SQL type name.
 * @throws SQLException If there is an error retrieving the SQL type name.
 * @Author David Hebert
 */
    @Override
    public String getSQLTypeName() throws SQLException {
        return Coupon.TYPENAME;
    }

}

