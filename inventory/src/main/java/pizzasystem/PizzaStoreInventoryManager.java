package pizzasystem;

import java.util.List;

public class PizzaStoreInventoryManager {
    
    private List<Pizza> pizzas;
    private ISorter iSorter;
    private IFilter iFilter; 
    private List<Coupon> coupon;


    /**
 * Constructs a PizzaStoreInventoryManager object with the specified parameters.
 *
 * @param pizzas   a list of pizzas to manage in the inventory
 * @param iFilter  an object implementing the IFilter interface for filtering pizzas
 * @param iSorter  an object implementing the ISorter interface for sorting pizzas
 * @Author Andrew Marks
 */
    public PizzaStoreInventoryManager(List<Pizza> pizzas, IFilter iFilter, ISorter iSorter){
        this.pizzas = pizzas;
        this.iFilter = iFilter;
        this.iSorter = iSorter;
    }


    /**
 * Adds a pizza to the inventory.
 *
 * @param pizza the pizza to be added to the inventory
 * @Author Andrew Marks
 */
    public void addPizza(Pizza pizza){
        pizzas.add(pizza);
    }


    /**
 * Removes a pizza from the inventory.
 *
 * @param pizza the pizza to be removed from the inventory
 * @Author Andrew Marks
 */
    public void removePizza(Pizza pizza){
        pizzas.remove(pizza);
    }

    
}
