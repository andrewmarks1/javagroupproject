package pizzasystem;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

public class FileDL{


    /**
 * Loads pizza data from a file and returns a list of Pizza objects.
 *
 * @return A List of Pizza objects loaded from the data file.
 * @throws IOException If an I/O error occurs.
 * @Author David Hebert
 */
    public List<Pizza> dataLoader() throws IOException {
        Path p = Paths.get("data.txt");
    
        List<String> lines = Files.readAllLines(p);
        List<Pizza> pizzas = new ArrayList<Pizza>();
    
        for (String line : lines) {
            List<String> toppings = new ArrayList<String>();
            String[] fields = line.split(",");
            
            String[] toppingArray = fields[3].split(" ");
    
            toppings.addAll(Arrays.asList(toppingArray));
    
            if (containsAny(toppings, "chicken", "bacon", "beef", "pepperoni", "sausage", "ham")) {
                pizzas.add(new Meat(fields[1], fields[2], toppings, Double.parseDouble(fields[4]), Boolean.parseBoolean(fields[5])));
            } else {
                pizzas.add(new Vegetarian(fields[1], fields[2], toppings, Double.parseDouble(fields[4]), Boolean.parseBoolean(fields[5])));
            }
        }
    
        return pizzas;
    }
    

    /**
 * Checks if a list of toppings contains any of the specified desired toppings.
 *
 * @param toppings The list of toppings to check.
 * @param desiredToppings The desired toppings to check for.
 * @return True if any desired topping is found in the list, otherwise false.
 * @Author David Hebert
 */
    private boolean containsAny(List<String> toppings, String... desiredToppings) {
        for (String desiredTopping : desiredToppings) {
            for (String topping : toppings) {
                if (topping.trim().equalsIgnoreCase(desiredTopping)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
    * Adds pizza data to a file.
    *
    * This method reads existing data from a file, appends the string representation
    * of the provided Pizza object, and writes the updated data back to the same file.
    * @Author David Hebert
    * @param pizza The Pizza object to be added to the file.
    * @throws IOException If an I/O error occurs while reading or writing the file.
    @Author David Hebert
    */
    public void addData(Pizza pizza, String path) throws IOException{
        Path p = Paths.get(path);
        List<String> temp = Files.readAllLines(p, StandardCharsets.UTF_8);
        temp.add(pizza.toString());
        Files.write(p, temp, StandardCharsets.UTF_8);
    }
    
}
