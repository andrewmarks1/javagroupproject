package pizzasystem;

import java.util.List;

public class Pizza {
    private List<String> toppings;
    private double price;
    private String size;
    private String name;
    private boolean gluten;


    /**
 * Constructs a Pizza object with the specified parameters.
 *
 * @param name     the name of the pizza
 * @param size     the size of the pizza
 * @param toppings a list of toppings for the pizza
 * @param price    the price of the pizza
 * @param gluten   a boolean indicating whether the pizza is gluten-free or not
 * @Author Andrew Marks
 */
    public Pizza(String name, String size, List<String> toppings, double price, boolean gluten){
        this.toppings = toppings;
        this.price = price;
        this.size = size;
        this.name = name;
        this.gluten = gluten;
    }

    /**
 * Sets the name of the pizza.
 *
 * @param name the name to be set for the pizza
 * @Author Andrew Marks
 */
    public void addName(String name){
        this.name = name;
    }


    /**
 * Retrieves the list of toppings for the pizza.
 *
 * @return a list of toppings for the pizza
 * @Author Andrew Marks
 */
    public List<String> getToppings(){
        return this.toppings;
    }


    /**
 * Sets the size of the pizza.
 *
 * @param size the size to be set for the pizza
 * @Author Andrew Marks
 */
    public void setSize(String size){
        this.size = size;
    }


    /**
 * Sets the price of the pizza.
 *
 * @param price the price to be set for the pizza
 * @Author Andrew Marks
 */

    public void setPrice(double price){
        this.price = price;
    }


    /**
 * Retrieves the size of the pizza.
 *
 * @return the size of the pizza
 * @Author Andrew Marks
 */
    public String getSize(){
        return this.size;
    }


    /**
 * Retrieves the name of the pizza.
 *
 * @return the name of the pizza
 * @Author Andrew Marks
 */
    public String getName(){
        return this.name;
    }


    /**
 * Retrieves the price of the pizza.
 *
 * @return the price of the pizza
 * @Author Andrew Marks
 */
    public double getPrice(){
        return this.price;
    }


    /**
 * Adds a topping to the pizza.
 *
 * @param topping the topping to be added
 * @Author Andrew Marks
 */
    public void addTopping(String topping){
        this.toppings.add(topping);
    }


    /**
 * Checks if the pizza is vegetarian.
 *
 * @return true if the pizza is vegetarian, false otherwise
 * @Author Andrew Marks
 */
    public boolean isVegetarian(){
        String[] meats = { "pepperoni", "sausage", "bacon", "chicken", "beef" };

        for(String topping : this.toppings){
            for(String s : meats){
                if(topping.contains(s)){
                    return false;
                }
            }
        }
        return true;
    }


    /**
 * Checks if the pizza is gluten-free.
 *
 * @return true if the pizza is gluten-free, false otherwise
 * @Author Andrew Marks
 */
    public boolean isGlutenFree(){
        return this.gluten;
    }


    /**
 * Returns a string representation of the Pizza object.
 *
 * @return a string containing information about the pizza
 * @Author Andrew Marks
 */
    @Override
    public String toString() {
        String toppingsString = String.join(", ", this.getToppings());
        return "Name:," + this.getName() + "," + this.getSize() + "," + toppingsString + "," + this.getPrice() + "," + isGlutenFree();
    }
    
}
