package pizzasystem;

import java.util.List;

public class Meat extends Pizza {

    /**
 * Constructs a Meat object with the specified parameters.
 *
 * @param name     the name of the pizza
 * @param size     the size of the pizza
 * @param toppings a list of toppings for the pizza
 * @param price    the price of the pizza
 * @param gluten   a boolean indicating whether the pizza is gluten-free or not
 * @Author Andrew Marks
 */
    public Meat(String name, String size, List<String> toppings, double price, boolean gluten){
        super(name, size, toppings, price, gluten);
    }


    /**
 * Constructs a Meat object based on an existing Pizza object.
 *
 * @param pizza the pizza object to create a Meat object from
 * @Author Andrew Marks
 */
    public Meat(Pizza pizza){
        super(pizza.getName(), pizza.getSize(), pizza.getToppings(), pizza.getPrice(), pizza.isGlutenFree());
    }


    /**
 * Adds a meat topping to the pizza.
 *
 * @param topping the meat topping to be added
 * @throws IllegalArgumentException if the specified topping is not a valid meat topping
 * @Author Andrew Marks
 */
    public void addMeat(String topping){
        String[] meat = {"chicken", "bacon", "beef", "pepperoni"};
        for(int i =0; i < meat.length; i++){
            if(!topping.equals(meat[i])){
                throw new IllegalArgumentException("Not a meat");
            }
        }
        this.addTopping(topping);
    }


    /**
 * Retrieves the meat toppings of the pizza.
 *
 * @return a string containing the meat toppings of the pizza
 * @Author Andrew Marks
 */
    public String getMeat(){
        String[] meats = {"chicken", "bacon", "beef", "pepperoni", "sausage", "ham"};
        String s = "";
        List<String> pizzaToppings = this.getToppings();
        for(String topping : pizzaToppings){
            for(String meat : meats){
                if(topping.contains(meat)){
                    s += meat;
                }
            }   
        }
        return s;
    }


    /**
 * Returns a string representation of the Meat object.
 *
 * @return a string containing information about the meat pizza
 * @Author Andrew Marks
 */
    @Override
    public String toString(){
        String toppingsString = String.join(" ", this.getToppings());
        return "Meat Pizza Name:," + this.getName() + "," + this.getSize() + "," + toppingsString + "," + this.getPrice() + "," + isGlutenFree();
    }
}
