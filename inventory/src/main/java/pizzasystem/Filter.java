package pizzasystem;

import java.util.List;
import java.util.ArrayList;
   
public class Filter implements IFilter {

    /**
 * Filters a list of pizzas based on the specified size.
 *
 * @param pizzas The list of pizzas to filter.
 * @param size The size to filter by (L, M, S).
 * @return A new list of pizzas with the specified size.
 * @Author Andrew Marks
 */
    public List<Pizza> filterBySize(List<Pizza> pizzas, String size){
        List<Pizza> newList = pizzas;
        if(size.equals("L")){
            newList = removeSize(pizzas, "M");
            newList = removeSize(pizzas, "S");
        }
        else if(size.equals("M")){
            newList = removeSize(pizzas, "L");
            newList = removeSize(pizzas, "S");
        }
        else if(size.equals("S")){
            newList = removeSize(pizzas, "L");
            newList = removeSize(pizzas, "M");
        }
        return newList;
    }


    /**
 * Filters a list of pizzas based on the specified type (Meat or Veg).
 *
 * @param pizzas The list of pizzas to filter.
 * @param type The type to filter by (Meat or Veg).
 * @return A new list of pizzas with the specified type.
 * @Author Andrew Marks
 */
    public List<Pizza> filterByType(List<Pizza> pizzas, String type) {
        List<Pizza> newList = new ArrayList<Pizza>();
        
        for (Pizza p : pizzas) {
            if ((type.equals("Meat") && p instanceof Meat) || (type.equals("Veg") && p instanceof Vegetarian)) {
                newList.add(p);
            }
        }
        
        return newList;
    }
    

    /**
 * Filters a list of meat pizzas based on the specified meat topping.
 *
 * @param pizzas The list of pizzas to filter.
 * @param meat The meat topping to filter by.
 * @return A new list of meat pizzas with the specified meat topping.
 * @Author Andrew Marks
 */
    public List<Pizza> filterByMeat(List<Pizza> pizzas, String meat) {
        List<Pizza> newList = new ArrayList<Pizza>();
        pizzas = filterByType(pizzas, "Meat");
    
        for (Pizza p : pizzas) {
            if (p.getToppings().contains(meat)) {
                newList.add(p);
            }
        }
    
        return newList;
    }
    
/**
 * Filters a list of pizzas based on the specified name.
 *
 * @param pizzas The list of pizzas to filter.
 * @param name The name to filter by.
 * @return A new list of pizzas with the specified name.
 * @Author Andrew Marks
 */
    public List<Pizza> filterByName(List<Pizza> pizzas, String name){
        List<Pizza> newList = new ArrayList<Pizza>();

        for(Pizza p : pizzas){
            if(p.getName().equals(name)){
                newList.add(p);
            }
        }
        return newList;
    }


    /**
 * Filters a list of pizzas based on the specified topping.
 *
 * @param pizzas The list of pizzas to filter.
 * @param topping The topping to filter by.
 * @return A new list of pizzas with the specified topping.
 * @Author Andrew Marks
 */
    public List<Pizza> filterByTopping(List<Pizza> pizzas, String topping){
        List<Pizza> newList = new ArrayList<Pizza>();
        for(Pizza p : pizzas){
            if(p.getToppings().contains(topping)){
                newList.add(p);
            }
        }
        return newList;
    }


    /**
 * Filters a list of pizzas based on the specified price.
 *
 * @param pizzas The list of pizzas to filter.
 * @param price The price to filter by.
 * @return A new list of pizzas with the specified price.
 * @Author Andrew Marks
 */
    public List<Pizza> filterByPrice(List<Pizza> pizzas, Double price){
        List<Pizza> newList = new ArrayList<Pizza>();
        for(Pizza p : pizzas){
            if(p.getPrice() == price){
                newList.add(p);
            }
        }
        return newList;
    }


    /**
 * Filters a list of pizzas based on the gluten-free attribute.
 *
 * @param pizzas The list of pizzas to filter.
 * @return A new list of gluten-free pizzas.
 * @Author Andrew Marks
 */
    public List<Pizza> filterByGluten(List<Pizza> pizzas){
        List<Pizza> newList = new ArrayList<Pizza>();

        for(Pizza p : pizzas){
            if(p.isGlutenFree()){
                newList.add(p);
            }
        }
        return newList;
    }


    /**
 * Removes pizzas of the specified size from the given list.
 *
 * @param pizzas The list of pizzas to remove from.
 * @param size The size to remove.
 * @return A new list of pizzas with the specified size removed.
 * @Author Andrew Marks
 */
    private List<Pizza> removeSize(List<Pizza> pizzas, String size){
        List<Pizza> pizzasToRemove = new ArrayList<Pizza>();
        for(Pizza p : pizzas){
            if(p.getSize().equals(size)){
                pizzasToRemove.add(p);
            }
        }
        pizzas.removeAll(pizzasToRemove);
        return pizzas;
    }
}
