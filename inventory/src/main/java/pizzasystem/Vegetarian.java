package pizzasystem;

import java.util.List;

public class Vegetarian extends Pizza{
    

    /**
 * Constructs a Vegetarian object with the specified parameters.
 *
 * @param name     the name of the vegetarian pizza
 * @param size     the size of the vegetarian pizza
 * @param toppings a list of toppings for the vegetarian pizza
 * @param price    the price of the vegetarian pizza
 * @param gluten   a boolean indicating whether the vegetarian pizza is gluten-free or not
 * @Author Andrew Marks
 */
    public Vegetarian(String name, String size, List<String> toppings, double price, boolean gluten){
        super(name, size, toppings, price, gluten);
    }


    /**
 * Constructs a Vegetarian object based on an existing Pizza object.
 *
 * @param pizza the pizza object to create a Vegetarian object from
 * @Author Andrew Marks
 */
    public Vegetarian(Pizza pizza){
        super(pizza.getName(), pizza.getSize(), pizza.getToppings(), pizza.getPrice(), pizza.isGlutenFree());
    }


    /**
 * Adds a vegetable topping to the vegetarian pizza.
 *
 * @param topping the vegetable topping to be added
 * @throws IllegalArgumentException if the specified topping is a meat topping
 * @Author Andrew Marks
 */
    public void addVegetable(String topping){
        String[] meat = {"chicken", "bacon", "beef", "pepperoni", "sausage"};
        for(int i =0; i < meat.length; i++){
            if(topping.equals(meat[i])){
                throw new IllegalArgumentException("Cannot add meat to a vegetarian pizza");
            }
        }
        this.addTopping(topping);
    }


    /**
 * Retrieves the vegetable toppings of the vegetarian pizza.
 *
 * @return a string containing the vegetable toppings of the vegetarian pizza
 * @Author Andrew Marks
 */
    public String getVegetables(){
        String[] vegetables = {"tomato", "mushroom", "pepper", "olives", "spinach", "onions"};
        String s = "";
        List<String> pizzaToppings = this.getToppings();
        for(String topping : pizzaToppings){
            for(String veg : vegetables){
                if(topping.contains(veg)){
                    s += veg;
                }
            }   
        }
        return s;
    }


    /**
 * Returns a string representation of the Vegetarian object.
 *
 * @return a string containing information about the vegetarian pizza
 * @Author Andrew Marks
 */
    @Override
    public String toString(){
        String toppingsString = String.join(", ", this.getToppings());
        return "Vegetarian pizza Name:," + this.getName() + "," + this.getSize() + "," + toppingsString + "," + this.getPrice() + "," + isGlutenFree();
    }
}
