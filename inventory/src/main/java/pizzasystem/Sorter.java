package pizzasystem;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;

public class Sorter implements ISorter {

/**
 * Sorts a list of pizzas based on their sizes in ascending or descending order.
 *
 * @param pizzas the list of pizzas to be sorted
 * @param order  the order in which to sort the pizzas ("asc" for ascending, "desc" for descending)
 * @return a new list of pizzas sorted by size
 * @Author Andrew Marks
 */
    public List<Pizza> sizeSorter(List<Pizza> pizzas, String order){
        List<Pizza> newList = new ArrayList<Pizza>();
        if(order.equals("asc")){
            newList = removeSize(pizzas, newList, "S");
            newList = removeSize(pizzas, newList, "M");
            newList = removeSize(pizzas, newList, "L");
        }
        else if(order.equals("desc")){
            newList = removeSize(pizzas, newList, "L");
            newList = removeSize(pizzas, newList, "M");
            newList = removeSize(pizzas, newList, "S");
        }
        return newList;
    }

    /**
 * Helper method to remove pizzas of a specific size from the original list and add them to a new list.
 *
 * @param pizzas   the list of pizzas to remove from
 * @param newList  the list to add pizzas to
 * @param size     the size of pizzas to remove
 * @return a new list of pizzas with removed sizes
 * @Author Andrew Marks
 */
    private List<Pizza> removeSize(List<Pizza> pizzas, List<Pizza> newList, String size) {
        List<Pizza> pizzasToRemove = new ArrayList<Pizza>();
        for (Pizza p : pizzas) {
            if (p.getSize().equals(size)) {
                newList.add(p);
                pizzasToRemove.add(p);
            }
        }
        pizzas.removeAll(pizzasToRemove);
        return newList;
    }

/**
 * Sorts a list of pizzas based on their prices in ascending or descending order.
 *
 * @param pizzas the list of pizzas to be sorted
 * @param order  the order in which to sort the pizzas ("asc" for ascending, "desc" for descending)
 * @return the same list of pizzas sorted by price
 * @Author Andrew Marks
 */

    public List<Pizza> priceSorter(List<Pizza> pizzas, String order) {
        if (order.equals("asc")) {
            pizzas.sort(Comparator.comparingDouble(Pizza::getPrice));
        } else if (order.equals("desc")) {
            pizzas.sort(Comparator.comparingDouble(Pizza::getPrice).reversed());
        }
        return pizzas;
    }
    
}